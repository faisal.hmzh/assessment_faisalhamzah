package router

import (
	"topup-service/controller"

	"github.com/gorilla/mux"
)

func Router() *mux.Router {
	router := mux.NewRouter()
	hrouter := router.PathPrefix("/api").Subrouter() //globarl url api

	hrouter.HandleFunc("/input-harga", controller.Inputharga).Methods("PUT")
	hrouter.HandleFunc("/check-harga", controller.Checkharga).Methods("GET")
	hrouter.HandleFunc("/topup", controller.Topup).Methods("POST")
	hrouter.HandleFunc("/saldo", controller.CheckSaldo).Methods("POST")
	hrouter.HandleFunc("/buyback", controller.Buyback).Methods("POST")

	return router
}
