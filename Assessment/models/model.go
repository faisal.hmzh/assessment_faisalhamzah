package models

import (
	"fmt"
	"strconv"
	"topup-service/config"

	"github.com/teris-io/shortid"
)

type MCheckharga struct {
	HargaBuyBack int `json:"harga_buyback"`
	HargaTopUp   int `json:"harga_topup"`
}

type MInputharga struct {
	AdminId      string `json:"admin_id"`
	HargaBuyBack int    `json:"harga_buyback"`
	HargaTopUp   int    `json:"harga_topup"`
}

type MTopup struct {
	Gram  string `json:"gram"`
	Harga string `json:"harga"`
	Norek string `json:"norek"`
}

type Rekening struct {
	Norek string  `json:"norek"`
	Saldo float32 `json:"saldo"`
}

type Buyback struct {
	Gram  float32 `json:"Gram"`
	Harga int     `json:"Harga"`
	Norek string  `json:"Norek"`
}

func Checkharga() MCheckharga {
	db := config.ConnectDB()
	defer db.Close()
	//sql
	sql := "select harga_topup,harga_buyback from tbl_harga"
	//execute sql
	rows, err := db.Query(sql)
	if err != nil {
		fmt.Println("error di exec db :", err)
	}
	defer rows.Close()
	var mch MCheckharga
	for rows.Next() {
		err = rows.Scan(&mch.HargaTopUp, &mch.HargaBuyBack) // ambil data lalu di unmarshal ke STRUC
		if err != nil {
			fmt.Println("error ambil data :", err)
		}
	}

	return mch
}

func Inputharga(inharga MInputharga) (string, string) {
	db := config.ConnectDB()
	defer db.Close()
	sql := "update tbl_harga set harga_topup = $1, harga_buyback = $2 where admin_id =$3"
	_, err := db.Exec(sql, inharga.HargaTopUp, inharga.HargaBuyBack, inharga.AdminId)
	if err != nil {
		fmt.Println("test error :", err)
	}
	fmt.Println(inharga)

	refid, _ := shortid.Generate()
	sError := ""
	//cek admin
	iAdmin := inharga.AdminId
	if iAdmin != "a001" {
		sError = "1"
	}
	return refid, sError
}

func Topup(topup MTopup) (string, string) {
	refid, _ := shortid.Generate()
	sError := ""
	//cek admin
	harga, _ := strconv.Atoi(topup.Harga)
	hargaTopup := Checkharga().HargaTopUp
	if harga == hargaTopup {
		//insert tbl_topup
		insertTopup(topup)
		//hitung saldo norek
		Saldo := getSaldoRek(topup.Norek)
		//update tbl_rek norek
		updateRekening(topup.Norek, Saldo)
	} else {
		sError = "1"
	}
	return refid, sError
}

func insertTopup(topup MTopup) {
	db := config.ConnectDB()
	defer db.Close()
	sql := "insert into tbl_topup(norek,gram,harga)VALUES($1,$2,$3)"
	_, err := db.Exec(sql, topup.Norek, topup.Gram, topup.Harga)
	if err != nil {
		fmt.Println("sql insert topup : ", err)
	}
}

func getSaldoRek(rek string) float32 {
	db := config.ConnectDB()
	defer db.Close()
	sql := "select sum(NULLIF(gram,'')::float) from tbl_topup where norek=$1"
	rows, err := db.Query(sql, rek)
	if err != nil {
		fmt.Println("error di exec db :", err)
	}
	defer rows.Close()
	var saldo float32
	for rows.Next() {
		err = rows.Scan(&saldo)
		if err != nil {
			fmt.Println("error ambil data :", err)
		}
	}
	return saldo
}

func updateRekening(Norek string, Saldo float32) {
	db := config.ConnectDB()
	defer db.Close()
	sql := "update tbl_rekening set saldo=$1 where norek=$2"
	_, err := db.Exec(sql, Saldo, Norek)
	if err != nil {
		fmt.Println("sql update rek : ", err)
	}
}

func CheckSaldo(rekening Rekening) (string, string) {
	refid, _ := shortid.Generate()
	sError := ""

	norek := rekening.Norek

	if norek == "r001" {
		CheckSaldoRek()
	} else {
		sError = "1"
	}

	return refid, sError
}

func CheckSaldoRek() Rekening {
	db := config.ConnectDB()
	defer db.Close()
	//sql
	sql := "select norek,round(saldo::decimal,1) from tbl_rekening"
	//execute sql
	rows, err := db.Query(sql)
	if err != nil {
		fmt.Println("error di exec db :", err)
	}
	defer rows.Close()
	var rek Rekening
	for rows.Next() {
		err = rows.Scan(&rek.Norek, &rek.Saldo) // ambil data lalu di unmarshal ke STRUC
		if err != nil {
			fmt.Println("error ambil data :", err)
		}
	}

	return rek
}

func InputBuyback(buyback Buyback) (string, string) {
	refid, _ := shortid.Generate()
	sError := ""
	//cek admin
	saldoGram := CheckSaldoRek().Saldo
	gram := buyback.Gram
	if saldoGram >= gram {
		//insert tbl_topup

		//hitung saldo norek
		Saldo := CheckSaldoRek().Saldo
		Saldo = Saldo - gram
		//update tbl_rek norek
		updateRekening(buyback.Norek, Saldo)
	} else {
		sError = "1"
	}
	return refid, sError
}
