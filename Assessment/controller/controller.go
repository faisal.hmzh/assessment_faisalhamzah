package controller

import (
	"encoding/json"
	"fmt"
	"net/http"
	"topup-service/models"
)

type responseCheckHarga struct {
	Error bool               `json:"error"`
	Data  models.MCheckharga `json:"data"`
}
type responseInputHargaS struct {
	Error  bool   `json:"error"`
	Reffid string `json:"reff_id"`
}
type responseInputHargaF struct {
	Error   bool   `json:"error"`
	Reffid  string `json:"reff_id"`
	Message string `json:"message"`
}

type responseCheckSaldo struct {
	Error bool            `json:"error"`
	Data  models.Rekening `json:"data"`
}

func Checkharga(w http.ResponseWriter, r *http.Request) {
	w.Header().Add("Content-Type", "application/json") // set header api to json
	res := responseCheckHarga{}
	res.Error = false
	res.Data = models.Checkharga()
	json.NewEncoder(w).Encode(res)
}

func Inputharga(w http.ResponseWriter, r *http.Request) {
	w.Header().Add("Content-Type", "application/json") // set header api to json
	var inharga models.MInputharga
	//ambil body json
	err := json.NewDecoder(r.Body).Decode(&inharga)
	if err != nil {
		fmt.Println("test error :", err)
	}

	errors, idref := "", ""
	//cek error
	idref, errors = models.Inputharga(inharga)
	if errors == "" { //respons sukses
		ress := responseInputHargaS{}
		ress.Error = false
		ress.Reffid = idref
		json.NewEncoder(w).Encode(ress)
	} else if errors == "1" { //respon error admin
		resf := responseInputHargaF{}
		resf.Error = true
		resf.Reffid = idref
		resf.Message = "Bukan admin"
		json.NewEncoder(w).Encode(resf)
	}

}

func Topup(w http.ResponseWriter, r *http.Request) {
	w.Header().Add("Content-Type", "application/json") // set header api to json
	var topup models.MTopup
	//ambil body json
	err := json.NewDecoder(r.Body).Decode(&topup)
	if err != nil {
		fmt.Println("test error :", err)
	}

	errors, idref := "", ""
	//cek error
	idref, errors = models.Topup(topup)
	if errors == "" { //respons sukses
		ress := responseInputHargaS{}
		ress.Error = false
		ress.Reffid = idref
		json.NewEncoder(w).Encode(ress)
	} else if errors == "1" { //respon error admin
		resf := responseInputHargaF{}
		resf.Error = true
		resf.Reffid = idref
		resf.Message = "harga tidak sama dengan harga topup saat ini"
		json.NewEncoder(w).Encode(resf)
	}

}

func CheckSaldo(w http.ResponseWriter, r *http.Request) {
	w.Header().Add("Content-Type", "application/json") // set header api to json
	var rekening models.Rekening
	//ambil body json
	err := json.NewDecoder(r.Body).Decode(&rekening)
	if err != nil {
		fmt.Println("test error :", err)
	}

	errors, idref := "", ""
	//cek error
	idref, errors = models.CheckSaldo(rekening)
	if errors == "" { //respons sukses
		res := responseCheckSaldo{}
		res.Error = false
		res.Data = models.CheckSaldoRek()
		json.NewEncoder(w).Encode(res)
	} else if errors == "1" { //respon error admin
		resf := responseInputHargaF{}
		resf.Error = true
		resf.Reffid = idref
		resf.Message = "norek tidak sesuai"
		json.NewEncoder(w).Encode(resf)
	}
}

func Buyback(w http.ResponseWriter, r *http.Request) {
	w.Header().Add("Content-Type", "application/json") // set header api to json
	var buyback models.Buyback
	//ambil body json
	err := json.NewDecoder(r.Body).Decode(&buyback)
	if err != nil {
		fmt.Println("test error :", err)
	}

	errors, idref := "", ""
	//cek error
	idref, errors = models.InputBuyback(buyback)
	if errors == "" { //respons sukses
		ress := responseInputHargaS{}
		ress.Error = false
		ress.Reffid = idref
		json.NewEncoder(w).Encode(ress)
	} else if errors == "1" { //respon error admin
		resf := responseInputHargaF{}
		resf.Error = true
		resf.Reffid = idref
		resf.Message = "Saldo buyback lebih kecil dari jumlah buyback"
		json.NewEncoder(w).Encode(resf)
	}
}
